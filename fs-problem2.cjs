const fs = require('fs');
const path = require('path');

function fsProblem2(lipsumPath) {
    readLipsumFile(lipsumPath)
        .then((result) => {
            return convertToUpperCase(result);
        })
        .then((upperCasePath) => {
            return convertToLowerCase(upperCasePath);
        })
        .then((lowerCasePath) => {
            return sortedContent(lowerCasePath);
        })
        .then((result) => {
            readAndDeleteAllFiles();
        })
        .catch((error) => {
            console.error(error.message);
        });
}

function readLipsumFile(directory) {
    return new Promise((resolve, reject) => {
        fs.readFile(directory, 'utf-8', (error, data) => {
            if (error) {
                reject(error);
            } else {
                resolve(data);
            }
        });
    });
}

function convertToUpperCase(lipsumData) {
    return new Promise((resolve, reject) => {
        const data = lipsumData.toUpperCase();
        const upperCasePath = path.resolve('../upperCase.txt');
        fs.writeFile(upperCasePath, data, (error) => {
            if (error) {
                reject(error);
            } else {
                fileNameCollector(upperCasePath);
                resolve(upperCasePath);
            }
        });
    });
}

function convertToLowerCase(filePath) {
    return new Promise((resolve, reject) => {
        fs.readFile(filePath, 'utf-8', (error, data) => {
            if (error) {
                reject(error);
            } else {
                const dataInLowerCase = data.toLowerCase();
                const splitData = dataInLowerCase.split('.');
                let lowerCaseData = splitData.join('\n');
                const lowerSplitDataPath = path.resolve('../lowerSplitFile.txt');
                fs.writeFile(lowerSplitDataPath, lowerCaseData, (error) => {
                    if (error) {
                        reject(error);
                    } else {
                        fileNameCollector(lowerSplitDataPath);
                        resolve(lowerSplitDataPath);
                    }
                });
            }
        });
    });
}

function sortedContent(lowerCasePath) {
    return new Promise((resolve, reject) => {
        fs.readFile(lowerCasePath, 'utf-8', (error, data) => {
            if (error) {
                reject(error);
            } else {
                let lines = data.split('\n');
                let sortedData = lines.sort();
                let finalData = sortedData.join('\n');
                const sortedDataPath = path.resolve('../sortedContent.txt');
                fs.writeFile(sortedDataPath, finalData, (error) => {
                    if (error) {
                        reject(error);
                    } else {
                        fileNameCollector(sortedDataPath);
                        resolve("Task Done Successfully");
                    }
                });
            }
        });
    });
}

function fileNameCollector(data) {
    const filePath = data + '\n';
    fs.appendFile("../filenames.txt", filePath, (error) => {
        if (error) {
            console.error(error);
            return;
        }
        else {
            console.log(`${data} added to filenames.txt`);
        }
    });
}

function readAndDeleteAllFiles() {
    new Promise((resolve, reject) => {
        fs.readFile('../filenames.txt', 'utf-8', (error, fileNamesData) => {
            if (error) {
                reject(error);
            } else {
                const fileNames = fileNamesData.split('\n').slice(0, 3);
                let count = 0;
                fileNames.map((fileName) => {
                    count++;
                    fs.rm(fileName, (error) => {
                        if (error) {
                            reject(error);
                        } else {
                            if (count === fileNames.length) {
                                resolve("All files deleted successfully.");
                            }
                        }
                    });
                });

            }

        });
    })
        .then((data) => {
            console.log(data);
            fs.unlink('../filenames.txt', (err) => {
                if (err) {
                    console.log(err);
                } else {

                    console.log("filesnames.txt deleted successfully.");
                }
            });
        });
}


module.exports = fsProblem2;