const fs = require('fs');

function fsProblem1(absolutePathOfRandomDirectory, randomNumberOfFiles) {
    createDirectoryAndFiles(absolutePathOfRandomDirectory, randomNumberOfFiles)
        .then((filesCreated) => {
            console.log(filesCreated);
            return deleteFilesAndDirectory(absolutePathOfRandomDirectory, randomNumberOfFiles)
        })
        .then((filesDeleted) => {
            console.log(filesDeleted);
        })
        .catch((error) => {
            console.error(error.message);
        });
}

function createDirectoryAndFiles(directory, randomNumberOfFiles) {
    return new Promise((resolve, reject) => {
        let fileCount = 0;
        fs.mkdir(directory, { recursive: true }, (error) => {
            if (error) {
                reject(error);
            } else {
                console.log("Directory created successfully!");
                if (randomNumberOfFiles === 0) {
                    resolve("Resolved..");
                }
                for (let index = 1; index <= randomNumberOfFiles; index++) {
                    fs.writeFile(`${directory}/randomFile-${index}.json`, "", (error) => {
                        if (error) {
                            reject(error);
                        } else {
                            fileCount++;
                            console.log("file created successfully", fileCount);
                            if (fileCount >= randomNumberOfFiles) {
                                console.log("All files created successfully!");
                                resolve("Resolved");
                            }
                        }
                    });
                }

            }
        });
    });

}

function deleteFilesAndDirectory(directory, randomNumberOfFiles) {
    console.log(randomNumberOfFiles);
    return new Promise((resolve, reject) => {
        if (randomNumberOfFiles === 0) {
            fs.rmdir(directory, (error) => {
                if (error) {
                    reject(error);
                } else {
                    console.log("Folder deleted successfully");
                    resolve("Files and Directory deleted successfully");
                }
            });
        }
        let filesDeleted = 0;
        for (let index = 1; index <= randomNumberOfFiles; index++) {
            let filePath = `${directory}/randomFile-${index}.json`;
            fs.unlink(filePath, (error) => {
                if (error) {
                    reject(error);
                } else {
                    console.log(`File no. ${index} removed successfully`);
                    filesDeleted++;
                    if (filesDeleted === randomNumberOfFiles) {
                        fs.rmdir(directory, (error) => {
                            if (error) {
                                reject(error);
                            } else {
                                console.log("Folder deleted successfully");
                                resolve("Files and Directory deleted successfully");
                            }
                        });
                    }
                }
            });
        }
    });
}

module.exports = fsProblem1;
